from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            # If you need to do something to the model before saving,
            # you can get the instance by calling
            # model_instance = form.save(commit=False)
            # Modifying the model_instance
            # and then calling model_instance.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    list = Task.objects.filter(assignee=request.user)
    context = {
        "list": list,
    }

    return render(request, "tasks/mine.html", context)
