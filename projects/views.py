from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)

    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    list_detail = Project.objects.get(id=id)
    items = Task.objects.filter(project=list_detail)
    context = {
        "list_detail": list_detail,
        "items": items,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            # If you need to do something to the model before saving,
            # you can get the instance by calling
            # model_instance = form.save(commit=False)
            # Modifying the model_instance
            # and then calling model_instance.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {"form": form}

    return render(request, "projects/create.html", context)
